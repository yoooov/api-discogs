<?php

class Discogs
{
	public $code;
	public $titre;
	public $auteur;
	public $date;
	public $style;
	public $genre;
	public $tracks;
	public $image;
	public $pays;
	public $url;

	public function searchById($id)
	{
		$this->code = $id;
		
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, "https://api.discogs.com/releases/".$id);
		curl_setopt($ch,CURLOPT_HTTPHEADER,array('User-Agent: APIBot/1.0'));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$output = curl_exec($ch);
		curl_close($ch);

		$data = json_decode($output, true);
		$this->titre = $data['title'];
		$this->date = $data['released'];
		$this->auteur = $data['artists'][0]['name'];
		$this->pays = $data['country'];
		$this->url = $data['uri'];

		$track = Array();
		foreach ($data['tracklist'] as $t) {
			$track[] = $t['position']." : ".$t['title']." (".$t['duration'].")";
		}
		$this->tracks = $track;

		foreach ($data['genres'] as $g) {
			$this->genre .= $g." ";
		}

		foreach ($data['styles'] as $s) {
			$this->style .= $s." ";
		}


	}

}



?>
